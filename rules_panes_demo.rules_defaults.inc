<?php
/**
 * @file
 * rules_panes_demo.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function rules_panes_demo_default_rules_configuration() {
  $items = array();
  $items['rules_mark_blocked'] = entity_import('rules_config', '{ "rules_mark_blocked" : {
      "LABEL" : "Mark blocked",
      "PLUGIN" : "rule",
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : { "task" : { "label" : "Task", "type" : "node" } },
      "IF" : [
        { "node_is_of_type" : { "node" : [ "task" ], "type" : { "value" : { "task" : "task" } } } }
      ],
      "DO" : [
        { "data_set" : { "data" : [ "task:field-status" ], "value" : "blocked" } }
      ]
    }
  }');
  $items['rules_mark_cancelled'] = entity_import('rules_config', '{ "rules_mark_cancelled" : {
      "LABEL" : "Mark cancelled",
      "PLUGIN" : "rule",
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : { "task" : { "label" : "Task", "type" : "node" } },
      "IF" : [
        { "node_is_of_type" : { "node" : [ "task" ], "type" : { "value" : { "task" : "task" } } } }
      ],
      "DO" : [
        { "data_set" : { "data" : [ "task:field-status" ], "value" : "cancelled" } }
      ]
    }
  }');
  $items['rules_set_task_ownership'] = entity_import('rules_config', '{ "rules_set_task_ownership" : {
      "LABEL" : "Set task ownership",
      "PLUGIN" : "rule",
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : {
        "task" : { "label" : "Task", "type" : "node" },
        "owner" : { "label" : "Owner", "type" : "user" }
      },
      "IF" : [
        { "node_is_of_type" : { "node" : [ "task" ], "type" : { "value" : { "task" : "task" } } } }
      ],
      "DO" : [
        { "data_set" : { "data" : [ "task:field-owner" ], "value" : [ "owner" ] } }
      ]
    }
  }');
  return $items;
}
