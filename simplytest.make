; Base parameters
; ---------------
api = 2
core = 7.x
projects[drupal][version] = 7.20
  
  
; Modules to work on
; ------------------
projects[] = rules_panes


; General-purpose modules
; -----------------------
projects[ctools][type] = "module"
projects[entity][type] = "module"
projects[entity][patch][] = "http://drupal.org/files/entity-node-access-1780646-16.patch"
projects[entityreference][type] = "module"
projects[features][type] = "module"
projects[flag][type] = "module"
projects[libraries][type] = "module"
projects[panels][type] = "module"
projects[rules][type] = "module"
projects[strongarm][type] = "module"
projects[views][type] = "module"
projects[views_bulk_operations][type] = "module"

; Special-purpose modules (and necessary patches)
; -----------------------------------------------
projects[panels_style_collapsible][type] = "module"

; Other modules
; -------------
projects[admin_menu][type] = "module"
projects[module_filter][type] = "module"
projects[devel][type] = "module"
projects[diff][type] = "module"

; Libraries
; ---------
